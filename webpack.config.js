var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/index.html',
    inject: 'body',
    favicon: 'src/favicon.ico'
});

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('dist'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [ 'babel-loader'],
                exclude: '/node_modules/'
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    },
    plugins: [
        HtmlWebpackPluginConfig,
        new CopyWebpackPlugin([ { from: 'data', to: 'data' } ])
    ]
};