# Uncertainty with D3

## Setup

```bash
brew bundle
yarn
yarn start
open http://localhost:8080
[yarn build]
```

## Workflow

Reorder dimensions with [ncdpg](http://nco.sourceforge.net/nco.html#xmp_ncpdq) to x,y,t (note: order is reverse):

```bash
ncpdq -a t,y,x C_std.nc C_std-xyt.nc
```

Convert netcdf to json with [ncdump-json](https://github.com/jllodra/ncdump-json):

```bash
$ ./ncdump-json -h -j C_std.nc
{"dimensions":{"t":9,"x":272,"y":64},"variables":{"t":{"type":"double","dimensions":["t"],"attributes":{"units":"d"}},"x":{"type":"double","dimensions":["x"],"attributes":{"units":"d"}},"y":{"type":"double","dimensions":["y"],"attributes":{"units":"d"}},"C_mean":{"type":"double","dimensions":["t","x","y"],"attributes":{}},"C_std":{"type":"double","dimensions":["t","x","y"],"attributes":{}}}}

$./ncdump-json -j -v t,[x,y,]C_mean,C_std C_std.nc > C_std.json
```

## Dependencies

- [D3.js](https://d3js.org)
- [gaussian](https://github.com/errcw/gaussian)
- [webpack](https://webpack.js.org/guides/)