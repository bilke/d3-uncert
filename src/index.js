// Imports and requires
import * as d3 from 'd3';
import legend from 'd3-svg-legend'
var gaussian = require('gaussian');

import fontawesome from '@fortawesome/fontawesome'
import { faPlay, faStop, faStepBackward, faStepForward } from '@fortawesome/fontawesome-free-solid'

import './index.css'

// ------------------------------------------------

var margin = {top: 10, right: 10, bottom: 20, left:30};
var width = 960 - margin.left - margin.right,
    height = 200 - margin.top - margin.bottom;

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var dataset,
    values =[],
    cells,
    scale = {
        x: d3.scaleBand().rangeRound([0, width]).align(0), // left-align, default is 0.5 which centers
        y: d3.scaleBand(),
        mean: d3.scaleLinear(),
        std: d3.scaleLinear(),
        y_line_std: d3.scaleLinear().rangeRound([height / 4, 0]),
        y_line_mean: d3.scaleLinear().rangeRound([height / 4, 0])
    },
    timestep = 0,
    numTimesteps = 0,
    meanRange = [], // [0.0, 0.1815],
    stdRange = [],// [0.0, 0.5266];
    animationDuration = 1000, // in ms
    timer,
    mode = "opacity",
    lineStd,
    lineMean,
    lineSlice,
    startIndex,
    endIndex;

    // global vars
    self.values = values;

d3.selectAll("#vis-controls input[name=mode]").on("change", function() {
    mode = this.value;
    SetTimestep(timestep);
});

d3.json("data/data-xyt.json", function (error, data) {
    if (error) throw error;
    dataset = data;

    // grid cells
    cells = { x: data.x.length, y: data.y.length };
    cells.data = d3.range(cells.x * cells.y)
        .map(function (i) {
            var x = i % cells.x,
                y = Math.floor(i / cells.x);
            return { x, y };
        });
    scale.x.domain(cells.data.map(function (d) { return d.x }));
    scale.y.domain(cells.data.map(function (d) { return d.y }))
        .rangeRound([cells.y * scale.x.step(), 0]);

    scale.x.roundedRange = [scale.x.range()[0], cells.x * scale.x.step()];

    meanRange = [d3.min(data.C_mean), d3.max(data.C_mean)];
    scale.y_line_mean.domain(meanRange);
    stdRange = [d3.min(data.C_std), d3.max(data.C_std)];
    meanRange[1] += 1 * stdRange[1];
    scale.mean.domain(meanRange);
    scale.std.domain(stdRange);

    scale.y_line_std.domain(stdRange);

    numTimesteps = data.t.length;
    var num_cells_x = data.x.length;
    var num_cells_y = data.y.length;
    var stride = num_cells_x * num_cells_y;
    for (var t = 0; t < numTimesteps; t++) {
        values.push(d3.transpose([
            data.C_mean.slice(t * stride, t * stride + stride),
            data.C_std.slice(t * stride, t * stride + stride)
        ]));

        values[t].forEach(function (d) {
            var numSigma = 3;
            var numSlices = 13; //numSigma * 10; // dependant on animation time; good values: 7 - 2s; 13 - 5s
            var mean = d[0];
            var std = d[1];
            var variance = std * std;
            var distribution =  null;
            if (variance > 0.0)
                distribution = gaussian(mean, variance);
            var domain = [0], range = [];

            var xarray = [];

            for (var slice = 0; slice < numSlices; slice++) {
                var x = - numSigma * std + mean + slice / numSlices * 2 * numSigma * std;
                var prob = slice / numSlices;
                if (distribution) {
                    prob = distribution.cdf(x);
                }
                domain.push(prob);
                range.push(GetColor(x));
                xarray.push(x);
            };

            d.push(d3.scaleThreshold().domain(domain).range(range));

            d.push(xarray);
        });
    }

    // grid
    var grid = svg.append("g")
        .attr("class", "grid");
    grid.selectAll("rect")
        .data(values[timestep])
        .enter()
        .append("rect")
        .attr("class", "grid")
        .attr("x", function (d, i) { return scale.x(cells.data[i].x); })
        .attr("y", function (d, i) { return scale.y(cells.data[i].y); })
        .attr("width", scale.x.step())
        .attr("height", scale.y.step())
        .attr("stroke-width", 0)
        .attr("fill", function (d) { return ColorValue(d, meanRange, stdRange); })
        .append("title")
        .text(function (d, i) { return "mean: " + d[0] + ", std: " + d[1] + ", index: " + i; });

    // line chart
    startIndex = data.x.length * Math.round(data.y.length / 2);
    endIndex = startIndex + data.x.length;
    lineStd = d3.line()
        .x(function (d, i) { return scale.x(cells.data[startIndex + i].x); })
        .y(function (d) {
            if (d[1] === undefined)
                return 0;
            else
                return scale.y_line_mean(d[1]); // scaled to mean
        });
    lineMean = d3.line()
        .x(function (d, i) { return scale.x(cells.data[startIndex + i].x); })
        .y(function (d) { return scale.y_line_mean(d[0]); });

    var lineDiv = d3.select("body").append("div");
    var lineSvg = lineDiv.append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", 70 + margin.top + margin.bottom)
        .append("g")
        .attr("class", "lineChart")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    lineSlice = values[timestep].slice(startIndex, endIndex);
    var lineGroup = lineSvg.append("g")
        .attr("class", "line");

    lineGroup.append("path")
        .data([lineSlice])
        .attr("class", "line std")
        .attr("d", lineStd);

    lineGroup.append("path")
        .data([lineSlice])
        .attr("class", "line mean")
        .attr("d", lineMean);

    // x axis
    var xAxis = d3.axisBottom(scale.x)
        .scale((d3.scaleLinear()
            .domain([data.x[0], data.x[num_cells_x-1]])
            .range(scale.x.roundedRange)));

    var y_offset = cells.y * scale.y.step() + 5;

    lineSvg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height / 4 + ")")
        .call(xAxis);

    // y axis
    // lineSvg.append("g")
    //     .attr("class", "axis")
    //     .call(d3.axisLeft(scale.y_line_std)
    //         .scale(scale.y_line_std)
    //         .ticks(3));

    lineSvg.append("g")
        //.attr("transform", "translate(" + scale.x.roundedRange[1] + ", 0)")
        .attr("class", "axis")
        .call(d3.axisLeft(scale.y_line_mean)
            .scale(scale.y_line_mean)
            .ticks(3));

    // line legend
    var ordinal = d3.scaleOrdinal()
        .domain(["mean", "std"])
        .range([ "steelblue", "red"]);

    lineSvg.append("g")
        .attr("class", "legendOrdinal")
        .attr("transform", "translate(" + (scale.x.roundedRange[1] - 45) + ", 10)");

    var legendOrdinal = legend.legendColor()
        .shape("path", d3.symbol().type(d3.symbolSquare).size(75)())
        .scale(ordinal);

    lineSvg.select(".legendOrdinal")
        .call(legendOrdinal);

    lineSvg.append("text")
        .attr("transform",
            "translate(" + (width/2 - margin.left - margin.right) + " ," +  (70 + margin.top) + ")")
        .attr("class", "small")
        .style("text-anchor", "middle")
        .text("Centered cross-section along x-domain");

    y_offset += 50;
    // time
    var timeDiv = d3.select("body").append("div").attr("class", "box");
    var timeSvg = timeDiv.append("svg")
        .attr("width", width / 2 - margin.left - margin.right)
        //.attr("width", "100%")
        .attr("height", 40 + margin.top + margin.bottom)
        .append("g")
        .attr("class", "time")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var timeScale = d3.scaleLinear()
        .domain([data.t[0], data.t[numTimesteps-1]])
        .range([scale.x.roundedRange[0], width / 2 - 2 * margin.left - 2 * margin.right]);

    var time = timeSvg.append("g")
        .attr("class", "chart");
    time.selectAll("circle")
        .data(data.t)
        .enter()
        .append("circle")
        .attr("class", "time")
        .attr("cx", function (d) { return timeScale(d); })
        .attr("cy", 0)
        .attr("r", 3)
        .attr("fill", function (d) {
            if (d === data.t[timestep])
                return "red";
            else
                return "black";
        })
        .on("click", function (d, i) {
            SetTimestep(i); // TOOD: Updating tooltip slow!
        })
        .on("mouseover", function () {
            d3.select(this)
                .attr("fill", "orange");
        })
        .on("mouseout", function () {
            d3.select(this)
                .transition()
                .attr("fill", function (d) {
                    if (d === data.t[timestep])
                        return "red";
                    else
                        return "black";
                });
        });

    time.selectAll("line")
        .data(data.t)
        .enter()
        .append("line")
        .attr("class", "time")
        .attr("x1", function (d) { return timeScale(d); })
        .attr("x2", function (d) { return timeScale(d); })
        .attr("y1", 2.5).attr("y2", 20)
        .attr("stroke", "black").attr("stroke-opacity", "0.25");

    timeSvg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(-.5," + 20 + ")")
        .call(d3.axisBottom().scale(timeScale));

    timeSvg.append("text")
        .attr("transform",
            "translate(" + (width/4 - margin.left - margin.right) + " ," +  (40 + margin.top) + ")")
        .attr("class", "small")
        .style("text-anchor", "middle")
        .text("Time");

    var controls = timeDiv.append("div").attr("class", "controls");

    controls.append("span").html(fontawesome.icon(faStepBackward).html)
        .attr("class", "button")
        .on("click", function () {
            if (playTimer) playTimer.stop();
            SetTimestep(timestep - 1);
        });
    controls.append("span").html(fontawesome.icon(faPlay).html)
        .attr("class", "button")
        .on("click", function () {
            playTimer = d3.interval(function () {
                SetTimestep(timestep + 1);
            }, 3000);
        });
    controls.append("span").html(fontawesome.icon(faStop).html)
        .attr("class", "button")
        .on("click", function () {
            if (playTimer) playTimer.stop();
        });
    controls.append("span").html(fontawesome.icon(faStepForward).html)
        .attr("class", "button")
        .on("click", function () {
            if (playTimer) playTimer.stop();
            SetTimestep(timestep + 1);
        });

    // legend
    // var legendDiv = d3.select("body").append("div").attr("style", "display:inline-block;");
    // var legendSvg = legendDiv.append("svg")
    //     .attr("width", 350)
    //     .attr("height", 60 + margin.top + margin.bottom)
    //     .append("g")
    //     .attr("class", "time")
    //     .attr("transform", "translate(" + margin.left + "," + (margin.top + 10) + ")");

    svg.append("g")
        .attr("transform", "translate(830, 20)")
        .attr("class", "legendMean");

    var meanScaleColor = d3.scaleSequential(d3.interpolateViridis)
        .domain(meanRange);

    var legendMean = legend.legendColor()
        //.title("Concentration [0, max(mean) + 1*max(std)]")
        .title("concentr.")
        .ascending(true)
        .shapeWidth(20)
        .cells(8)
        //.orient("horizontal")
        //.labelFormat(".1")
        .labels(function ({i, genLength, generatedLabels}) {
            if (i === 0 || i === genLength-1)
                return generatedLabels[i];
            else return "";
        })
        .scale(meanScaleColor);

    svg.select(".legendMean").call(legendMean);

    // TODO: histogram
    //var hist = histogram(data.C_std, 300, 250);
    //hist.attr("transform", "translate(0, 300");
});

var playTimer;

function SetTimestep(new_timestep) {
    if (new_timestep < 0) new_timestep = numTimesteps - 1;
    timestep = new_timestep % numTimesteps;

    if (timer)
        timer.stop();

    if (mode === "opacity") {
        svg.selectAll("rect.grid")
            .data(values[timestep])
            .transition()
            .duration(animationDuration)
            .attr("fill", function (d) {
                return ColorValue(d, meanRange, stdRange);
            });
    }
    else if (mode === "animation") {
        timer = d3.interval(function (time) {

            var animLength = 5000, // ms
                normTime = time % animLength / animLength;

            // Ping-pong
            //if((time % (animLength * 2)) > animLength)
            //    normTime = 1 - normTime;

            svg.selectAll("rect.grid")
                .data(values[timestep])
                // .filter(function (d) { return d[1] > 0.0001; }) // first frame of timestep should not filter!
                .attr("fill", function (d) {
                    return d[2](normTime);
                });
        }, 10);
    }

    // TODO: slow
    //svg.selectAll("rect.grid>title")
    //    .data(values[timestep])
    //    .text(function (d) {
    //        return "mean: " + d[0] + ", std: " + d[1];
    //    });


    var lineSlice = values[timestep].slice(startIndex, endIndex);
    d3.selectAll("path.line.std")
        .data([lineSlice])
        .transition()
        .duration(animationDuration / 2)
        .attr("d", lineStd);

    console.log(d3.selectAll("path.line, path.mean"));
    d3.selectAll("path.line.mean")
        .data([lineSlice])
        .transition()
        .duration(animationDuration / 2)
        .attr("d", lineMean);

    d3.selectAll("circle.time")
        .data(dataset.t)
        .transition()
        .attr("fill", function (d) {
            if (d === dataset.t[timestep])
                return "red";
            else
                return "black";
        });
}

function GetColor(d) { return d3.color(d3.interpolateViridis(scale.mean(d))); }

function ColorValue(d, colorRange, opacityRange) {
    var color = GetColor(d[0]);
    color.opacity = 1 - (scale.std(d[1]) / 2);
    return color;
}

function histogram(data, width, height) {
    console.log(d3.extent(data));
    console.log(data);
    var g = svg.append("g")
        .attr("class", "hist");

    var x = d3.scaleLinear()
        .domain(d3.extent(data))
        .rangeRound([0, width]);

    var histogram = d3.histogram()
        .domain(x.domain)
        .thresholds(x.ticks(10));
    var bins = histogram([0, 0.1, 0.1, 0.2]);
    console.log(bins);

    var y = d3.scaleLinear()
        .domain([0, d3.max(bins, function (d) { return d.length; })])
        .range([height, 0]);

    var bar = g.selectAll("rect.bar")
        .data(bins)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", 1)
        .attr("transform", function(d) {
            return "translate(" + x(d.x0) + "," + y(d.length) + ")";
        })
        .attr("width", function (d) { return x(d.x1) - x(d.x0) -1; })
        //.attr("width", "20")
        .attr("height", function(d) { return height - y(d.length); })
        .attr("fill", "steelblue");

    g.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    return g;
}